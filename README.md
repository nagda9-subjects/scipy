# SciPy

Documentation for PettingZoo:\
<https://pettingzoo.farama.org/tutorials/custom_environment/>\
<https://pettingzoo.farama.org/content/environment_creation/>\
<https://pettingzoo.farama.org/api/aec/>\
<https://pettingzoo.farama.org/content/basic_usage/>\
<https://pettingzoo.farama.org/api/wrappers/pz_wrappers/>

Documentation for Gym: (PettingZoo builds on it)\
<https://gymnasium.farama.org/>

Documentation for SB3 DQN RL Agent:\
<https://stable-baselines3.readthedocs.io/en/master/modules/dqn.html>

More RL Agents on SB3 Contrib:\
<https://sb3-contrib.readthedocs.io/en/master/>

Encoding Observation Space:\
<https://towardsdatascience.com/3-key-encoding-techniques-for-machine-learning-a-beginner-friendly-guide-aff8a01a7b6a>

## Skull: a 2-6 player bluffing card game

![Skull](./docs/skull.jpeg)

### Game Setup

Each player have a square mat, and 4 circular mats. The square mat have a colourful and a dark side,
it has to be placed down with the colorful side facing up. The 4 circular mats consist of three flower
cards, and one skull card.

### Rules

Each player starts with the 4 circular mats that they keep secret: three of them are flowers; one is a skull.
Everyone simultaneously picks one of their cards and places it face-down onto their square mat. Let’s imagine
a five-player game is taking place. There will now be five cards sitting face-down around the table.

The start player now has a choice. They can either:

1. Play another card face-down on top of their first card, or…
2. Open the bidding, by vocally stating how many flower cards they think they can turn over in a row
   among those that have been collectively played so far (as in, uninterrupted, without revealing a skull card).

In the case of option one, play resumes to the player to their left, who now has the same choices:
play another card face-down, or start the bidding. Once a player decides to open the bidding,
no more cards can be added face-down – now the players’ only option is option two.

In the case of option two, the player has opened the bidding. Play resumes to the player on their left,
who now has two options. They can either:

1. Raise the bid, if they think they can turn over more flower cards without seeing a skull, or…
2. Pass, in which case they sit out the remainder of the round.

Play resumes in a clockwise nature, where the ante is constantly raised until all other players
but one has passed, which leaves one player – now known as The Challenger – with the highest bid.

Now comes the fun part! Whenever a player has outbid everyone with a number, it’s now time for them to start
turning over cards they think are flowers. As soon as this Challenger turns over a skull in this process,
they have failed and the round ends. No more cards are revealed.

If the Challenger reveals a skull, they fail. They’ll probably shake their fist at the person that played
that skull, and that person will then, blindly, remove one of the Challenger’s cards. It’s returned to the
box and not revealed to anyone. Everyone takes back their cards from the square mat and play repeats
(go back to Step 1). The Challenger that just failed with their attempt is now the start player.

A player is eliminated from the game if they lose their fourth and final card.

If, however, the Challenger manages to turn over their stated number of cards and they’re all flowers,
they win the round. They turn their square mat over (now dark side is up) as a visual reminder that they
are now 50% of the way to winning! Everyone takes back their cards and play repeats (go back to Step one).
The Challenger that just won the round with their attempt is now the start player.

Therefore, the first player to achieve the above twice is the winner.

However! There is a wonderful twist in this tale. The Challenger with the highest bid has to turn over x
number of flowers (x being their bid). They can decide which order they wish to turn over cards, BUT they must
always start by turning over their OWN cards, first. If one of these was a skull
(because they might have been bluffing/baiting another player into bidding higher), they instantly fail.

Once the Challenger has (successfully) revealed their own cards, they can then start turning over the other
players’ cards, hoping to reveal flowers. In the occurrence that other players have played more than one card
face-down, if the Challenger wants to turn over one of their cards, they have to turn over the top-placed card
first, before those played underneath it. They can opt to only turn over the top card and leave the rest if
they wish, perhaps returning to them later.

There is one final thing to remember. In rare occasions, no player is able to triumph by winning two rounds,
due to copious numbers of skulls thwarting attempts. It is possible to instead win by being the last player
standing, because everyone else has lost their cards.

In some circumstances, a player might find themselves with only one card left. If it is a skull, then if they
later become the Challenger, they have no choice but to turn it over and eliminate themselves. The only way they
can win in this situation is via the rare point made above, in which they are the last player standing.

So, in a nutshell: players try to bluff each other by secretly playing cards – a skull or one of their flowers.
They’ll then bet on how many flowers they think they can turn over. The winner is the first player to successfully
win two rounds, in which they indeed reveal their predicted number of flowers.
