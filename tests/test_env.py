import sys
import unittest

sys.path.append("../")
from skull_env import skull_v0


class test_basic_props(unittest.TestCase):
    def test_player_number_assert(self):
        """If player number is incorrect, there should be an assert."""
        self.assertRaises(Exception, skull_v0.env, num_players=0)
        self.assertRaises(Exception, skull_v0.env, num_players=1)
        self.assertRaises(Exception, skull_v0.env, num_players=7)

    def test_space_sampling(self):
        """
        When sampling the spaces with seeds, the actions and observations should be the same after reset.
        Each agent should generate different samples.
        """
        seed = 42
        env = skull_v0.env(num_players=2, seed=seed)

        a0l1 = []
        a0l2 = []
        a1l1 = []
        a1l2 = []
        o0l1 = []
        o0l2 = []
        o1l1 = []
        o1l2 = []
        for _ in range(20):
            a0_action = env.action_space(0).sample(mask=None)
            a1_action = env.action_space(1).sample(mask=None)
            a0l1.append(a0_action)
            a1l1.append(a1_action)

            a0_obs = env.observation_space(0).sample()
            a1_obs = env.observation_space(1).sample()
            o0l1.append(a0_obs)
            o1l1.append(a1_obs)

        env.reset(seed=seed)
        for _ in range(20):
            a0_action = env.action_space(0).sample(mask=None)
            a1_action = env.action_space(1).sample(mask=None)
            a0l2.append(a0_action)
            a1l2.append(a1_action)

            a0_obs = env.observation_space(0).sample()
            a1_obs = env.observation_space(1).sample()
            o0l2.append(a0_obs)
            o1l2.append(a1_obs)

        self.assertEqual(a0l1, a0l2)
        self.assertEqual(a1l1, a1l2)
        self.assertNotEqual(a0l1, a1l1)

        for i in range(20):
            self.assertEqual(o0l1[i].all(), o0l2[i].all())
            self.assertEqual(o1l1[i].all(), o1l2[i].all())

    def test_agent_iter_stop_when_empty(self):
        """When playing an invalid action the players should drop out in their next turn."""
        seed = 42
        for n_players in range(2, 7):
            env = skull_v0.env(num_players=n_players, seed=seed)

            n_steps = 0
            for _ in env.agent_iter(max_iter=20):
                _, _, termination, truncation, _ = env.last(observe=True)
                if termination or truncation:
                    action = None
                else:
                    action = 26  # Any invalid action
                env.step(action)
                n_steps += 1
            self.assertEqual(n_steps, 2 * n_players - 1)

    def test_stepping(self):
        """
        When a player drops out, the next player should continue the game,
        and the dropped out agent should not have any more turns.
        When SETUP or PLACING phase, player turns should iterate.
        When in CHLALLANGE or BIDDING phase, the active players should iterate.
        When only 1 player left, game should be over.
        """
        seed = 42

        env = skull_v0.env(num_players=2, seed=seed)
        # turns1 = []
        # agents_list1 = []
        # for agent in env.agent_iter(max_iter=10):
        #     observation, _, termination, truncation, _ = env.last()
        #     if termination or truncation:
        #         action = None
        #     else:
        #         if isinstance(observation, dict) and "action_mask" in observation:
        #             mask = observation["action_mask"]
        #         else:
        #             mask = None
        #         action = env.action_space(agent).sample(mask if agent == 1 else None)
        #     turns1.append(agent)
        #     agents_list1.append(env.agents)
        #     env.step(action)
        # for i in range(len(agents_list1) - 1):
        #     if len(agents_list1[i + 1]) < len(agents_list1[i]):
        #         self.assertNotIn(turns1[i], agents_list1[i + 1])
        #         self.assertEqual(turns1[i + 1], agents_list1[i + 1][0])

        # TODO write proper tests here


if __name__ == "__main__":
    unittest.main()
