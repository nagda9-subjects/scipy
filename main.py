from skull_env import skull_v0
from stable_baselines3 import DQN

# Add players
agents = [
    "DQN1",
    "DQN2",
    # "DQN3",
    # "DQN4",
    # "DQN5",
    "DQN6",
]  # replace agents with RL or custom agents

# Init env
seed = 42
rendering_mode = "gui"  # "text"
env = skull_v0.env(num_players=len(agents), seed=seed, render_mode=rendering_mode)
env.reset(seed=seed)

# model = DQN("MlpPolicy", env, verbose=1)
# model.learn(total_timesteps=10000, log_interval=4)
# model.save("dqn_skull")

for agent in env.agent_iter(max_iter=1000):
    """
    It terminates when max_iter steps have been executed, or when all agents are done (truncated or terminated),
    and therefore the agents list is empty.
    """
    if rendering_mode == "text":
        print(env.render())
        # with open("example.txt", "a") as file:
        #     file.write(env.render())
    elif rendering_mode == "gui":
        print(env.render())
        # time.sleep(5)

    # Wait for 5 seconds

    observation, reward, termination, truncation, info = env.last()
    if termination or truncation:
        action = None
    else:
        if "action_mask" in info:
            mask = info["action_mask"]
        elif isinstance(observation, dict) and "action_mask" in observation:
            mask = observation["action_mask"]
        else:
            mask = None
        action = env.action_space(agent).sample(mask)
    env.step(action)
env.close()
