import tkinter as tk
from tkinter import scrolledtext
from PIL import Image, ImageDraw, ImageTk, ImageFont
import io
import math
import os

class SkullGameGUI:
    def __init__(self, master, game_state):
        self.master = master
        self.game_state = game_state
        self.create_widgets()

    def create_widgets(self):
        agent, last_action, infos, cumulative_rewards, terminations, truncations, phase, num_placed_mats, largest_bid, num_active_players, num_revealed_mats, agents, players, action_mask = self.game_state
        
        # Draw the game state image
        self.image = self.draw_game_state()
        
        # Convert the image to a format that Tkinter can use
        self.tk_image = ImageTk.PhotoImage(self.image)
        
        # Create a canvas to display the image
        self.canvas = tk.Canvas(self.master, width=self.image.width, height=self.image.height)
        self.canvas.pack()
        
        # Add the image to the canvas
        self.canvas.create_image(0, 0, anchor=tk.NW, image=self.tk_image)
        
        self.continue_var = tk.BooleanVar()
        self.master.bind('<space>', self.on_space_press)
        tk.Button(self.master, text="Continue", command=self.continue_game).pack()

        #self.master.protocol("WM_DELETE_WINDOW", self.on_closing)

    def draw_game_state(self):
        # Load the background image
        curr_dir = os.path.abspath(os.path.dirname(__file__))
        img_dir = os.path.join(curr_dir, os.pardir, "docs")
        img_path = os.path.join(img_dir, "board_background.png")

        background_image = Image.open(img_path)
        
        # Create an image with the same size as the background
        width, height = background_image.size
        image = Image.new('RGB', (width, height))
        image.paste(background_image, (0, 0))
        
        draw = ImageDraw.Draw(image)
        
        # Calculate the positions for the players in a circle
        center_x, center_y = width // 2, height // 2
        radius = min(center_x, center_y) - 50
        num_players = len(self.game_state[11])
        
        # Define color options for player circles
        player_colors = ['#FF8888', '#88FF88', '#8888FF', '#FFFF88', '#FF88FF', '#88FFFF']
        font = ImageFont.truetype("arial.ttf", 16)

        for i, player_id in enumerate(self.game_state[11]):
            angle = 0
            if len(self.game_state[11]) == 3:
                angle = +15
            angle += 2 * math.pi * i / num_players
            x = center_x + radius * 0.8 * math.cos(angle)
            y = center_y + radius * 0.8 * math.sin(angle)
            player_data = self.game_state[12][player_id]
            
            # Draw the player circle with different colors for each player
            player_color = player_colors[i]
            if self.game_state[0] == player_id:
                draw.ellipse((x-30, y-30, x+30, y+30), fill=player_color, outline='black',width = 4)
            else:
                draw.ellipse((x-30, y-30, x+30, y+30), fill=player_color, outline='black')
            
            # Draw the player ID
            draw.text((x-10, y-10), f'P{player_id}', fill='black', font=font)
            
            # Draw the player data
            text_y = y + 40
            for key, value in player_data.items():
                if key != 'bid' and key != 'placed_mats' and key!= 'points' and key != 'skulls' and key!= 'flowers':
                    if not (key=='passed' and value == False):
                        draw.text((x-30, text_y), f'{key}: {value}', fill='black')
                        text_y += 15
            
            # Draw mats towards the center of the circle
            mat_radius = 10
            mat_angle = angle + math.pi / 12  # Adjust angle for drawing mats towards the center
            
            for mat in player_data['placed_mats']:
                mat_x = x - radius * 0.14 * math.cos(mat_angle)
                mat_y = y - radius * 0.14 * math.sin(mat_angle)
                if mat == 'flower':
                    draw.ellipse((mat_x - mat_radius, mat_y - mat_radius, mat_x + mat_radius, mat_y + mat_radius), fill='green', outline='black')
                    draw.text((mat_x - 5, mat_y - 7), "*", fill='black')  # Simple flower representation
                elif mat == 'skull':
                    draw.ellipse((mat_x - mat_radius, mat_y - mat_radius, mat_x + mat_radius, mat_y + mat_radius), fill='red', outline='black')
                    draw.line((mat_x - mat_radius//2, mat_y - mat_radius//2, mat_x + mat_radius//2, mat_y + mat_radius//2), fill='black', width=2)
                    draw.line((mat_x + mat_radius//2, mat_y - mat_radius//2, mat_x - mat_radius//2, mat_y + mat_radius//2), fill='black', width=2)
                mat_angle -= math.pi / 12  # Adjust angle for next mat

            mat_angle = angle + math.pi / 12
            for _ in range(player_data['flowers'] - player_data['placed_mats'].count('flower')):
                mat_x = x + radius * 0.14 * math.cos(mat_angle)
                mat_y = y + radius * 0.14 * math.sin(mat_angle)
                draw.ellipse((mat_x - mat_radius, mat_y - mat_radius, mat_x + mat_radius, mat_y + mat_radius), fill='green', outline='black')
                draw.text((mat_x - 5, mat_y - 7), "*", fill='black')  # Simple flower representation
                mat_angle -= math.pi / 12  # Adjust angle for next mat
            for _ in range(player_data['skulls'] - player_data['placed_mats'].count('skull')):
                mat_x = x + radius * 0.14 * math.cos(mat_angle)
                mat_y = y + radius * 0.14 * math.sin(mat_angle)
                draw.ellipse((mat_x - mat_radius, mat_y - mat_radius, mat_x + mat_radius, mat_y + mat_radius), fill='red', outline='black')
                draw.line((mat_x - mat_radius//2, mat_y - mat_radius//2, mat_x + mat_radius//2, mat_y + mat_radius//2), fill='black', width=2)
                draw.line((mat_x + mat_radius//2, mat_y - mat_radius//2, mat_x - mat_radius//2, mat_y + mat_radius//2), fill='black', width=2)
                mat_angle -= math.pi / 12
        # Draw other game state info
        info_text = f'Phase: {self.game_state[6]}\nPlaced Mats: {self.game_state[7]}\nLargest Bid: {self.game_state[8]}\nActive Players: {self.game_state[9]}\nRevealed Mats: {self.game_state[10]}'
        for player_id, player_data in self.game_state[12].items():
            info_text += f'\nPlayer {player_id} Points: {player_data["points"]}'
            info_text += f'\nPlayer {player_id} Bids: {player_data["bid"]}'
        draw.text((35, 35), info_text, fill='black', font=font)
        
        return image
    
    def on_space_press(self, event):
        self.continue_game()

    def continue_game(self):
        self.continue_var.set(True)

    #def on_closing(self):
    #    self.continue_var.set(True)
    #    self.master.quit()

def call_gui(game_state):
    root = tk.Tk()
    root.title("Skull Game State")
    app = SkullGameGUI(root, game_state)
    root.wait_variable(app.continue_var)
    root.destroy()
