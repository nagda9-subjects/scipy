"""
# Environment arguments

``` python
skull_v0.env(num_players=6, seed=42, render_mode=None)
```

`num_players`: Number of players, value between 2-6.
`seed`: Pseudo random number generation, space sampling.
'render_mode': TODO implement

# Observation Space

Note: This actually does not matter currently, later rewrite it to represent the necessary information in MultiBinary.
For this we can use make_column_transformer from sklearn.
For readability, just use a dictionary with the necessary data.

Maybe something like this?

|     ID    | Observation                                 |
|:---------:|---------------------------------------------|
|     0-3   | Phase (one hot encoded)                     |
|     4-8   | Placed mats by player1 (one hot encoded)    |
|    8-12   | Placed mats by player2 (one hot encoded)    |
|   12-16   | Placed mats by player3 (one hot encoded)    |
|   16-20   | Placed mats by player4 (one hot encoded)    |
|   20-24   | Placed mats by player5 (one hot encoded)    |
|   24-28   | Placed mats by player6 (one hot encoded)    |
|     4     | Number of mats in player1's hand (one hot)  |
|     4     | Number of mats in player2's hand (one hot)  |
|     4     | Number of mats in player3's hand (one hot)  |
|     4     | Number of mats in player4's hand (one hot)  |
|     4     | Number of mats in player5's hand (one hot)  |
|     4     | Number of mats in player6's hand (one hot)  |
|     6     | Number of points each player have (0 or 1)  |
|     6     | Bids by players if it was bigger than       |
|           | zero --> 1                                  |                                      

# Action Space

The action space is a scalar value, which ranges from 0 to the max number of actions. The values represent
all possible actions a player can make, legal or not.

|     ID    | Action                       |
|:---------:|------------------------------|
|     0     | Place a skull                |
|     1     | Place a flower               |
|     2     | Pass                         |
|   3-20    | Bid 1 - 18                   |
|   21-26   | Reveal 0 - 5                 |

Handle invalid actions: agent drops out from game, gets negative reward, and round is reset.

# Rewards

If player makes an illegal move, it gets a big negative reward, and loses.
"""

from pettingzoo import AECEnv
from pettingzoo.utils import wrappers, agent_selector

from gymnasium.utils import EzPickle
from gymnasium.spaces import Space, Discrete, MultiBinary

import numpy as np
from enum import Enum
from typing import Dict, Any
from pprint import pformat
from itertools import compress

from skull_env import gui

actions = {
    0: "Place a skull",
    1: "Place a flower",
    2: "Pass",
    3: "Bid 1",
    4: "Bid 2",
    5: "Bid 3",
    6: "Bid 4",
    7: "Bid 5",
    8: "Bid 6",
    9: "Bid 7",
    10: "Bid 8",
    11: "Bid 9",
    12: "Bid 10",
    13: "Bid 11",
    14: "Bid 12",
    15: "Bid 13",
    16: "Bid 14",
    17: "Bid 15",
    18: "Bid 16",
    19: "Bid 17",
    20: "Bid 18",
    21: "Reveal 0",
    22: "Reveal 1",
    23: "Reveal 2",
    24: "Reveal 3",
    25: "Reveal 4",
    26: "Reveal 5",
}


class Phase(Enum):
    SETUP = 0
    PLACING = 1
    BIDDING = 2
    CHALLENGE = 3


class raw_env(AECEnv, EzPickle):
    """
    The Scull Environment class without wrappers.
    It implements the AECEnv interface for multiplayer turn based actions.
    It implements EzPickle for seeding.
    """

    def __init__(self, num_players=0, seed=None, render_mode=None):
        """
        Constructor, initialize private attributes.
        """

        """
        Init EzPickle. This class helps when making a copy of the env.
        Pass on every argument from the constructor parameters.
        """
        EzPickle.__init__(
            self,
            num_players=num_players,
            seed=seed,
            render_mode=render_mode,
        )

        # Assert that the given parameters are valid
        if not (num_players >= 2 and num_players <= 6):
            raise Exception(
                "Wrong player number was given: "
                + str(num_players)
                + ". Required number of players is 2-6."
            )

        """
        The metadata holds environment constants.
        The "name" metadata allows the environment to be pretty printed.
        """
        self.metadata = {
            "name": "skull_env_v0",
            "render_modes": ["text"],
            "is_parallelizable": False,
        }

        """
        A list of all possible agents the environment could generate.
        Equivalent to the list of agents in the observation and action spaces.
        This cannot be changed through play or resetting.
        """
        self.possible_agents = list(range(num_players))

        """This is keeping track of the active players. If a player drops out, or comes back to play, this list changes."""
        self.agents = self.possible_agents[:]

        # """
        # A dict of the rewards of every current agent at the time called, keyed by name.
        # It is the instantaneous reward generated after the last step.
        # Note that agents can be added or removed from this attribute.
        # """
        # self.rewards = {agent: 0 for agent in self.agents}

        """The gained reward during the episode."""
        self._cumulative_rewards = {agent: 0 for agent in self.agents}

        """
        A dict of the termination state of every current agent at the time called, keyed by name.
        Signals if the agent has finished, because it reached a terminal state (e.g. win).
        """
        self.terminations = {agent: False for agent in self.agents}

        """Signals if the agent has finished, due to some external condition (e.g. episode reached max num of turns)"""
        self.truncations = {agent: False for agent in self.agents}

        """
        A dict of info for each current agent, keyed by name.
        Each agent’s info is also a dict. 
        Note that agents can be added or removed from this attribute.
        It can contain debug or warning messages.
        """
        self.infos = {agent: {"prev_action": None} for agent in self.agents}

        """Way of displaying the game. Possible options listed in metadata.render_modes."""
        self.render_mode = render_mode

        """Iterator to cycle through the players"""
        self._agent_selector = agent_selector(self.agents)

        """An attribute of the environment corresponding to the currently selected agent that an action can be taken for."""
        self.agent_selection = self._agent_selector.reset()

        self._last_action = None

        """
        Seed the random generators.
        Initialize observation_spaces and action_spaces with seed.
        This has to be done AFTER possible_agents was initialized!
        """
        self.seed, self.observation_spaces, self.action_spaces = self._seed(seed=seed)

        """Env specific variables"""
        (
            self.phase,
            self.players,
            self.num_placed_mats,
            self.largest_bid,
            self.num_active_players,  # players who has passed at bidding
            self.num_revealed_mats,
        ) = self._init_env_state()

    def reset(self, seed=None, options=None):  # complete reset of the game
        """
        Reset needs to initialize the following attributes
        - agents
        # - rewards
        - _cumulative_rewards
        - terminations
        - truncations
        - infos
        - agent_selection
        And must set up the environment so that render(), step(), and observe()
        can be called without issues.
        Here it sets up the state dictionary which is used by step() and the observations dictionary which is used by step() and observe()
        """
        self.agents = self.possible_agents[:]
        # self.rewards = {agent: 0 for agent in self.agents}
        self._cumulative_rewards = {agent: 0 for agent in self.agents}
        self.terminations = {agent: False for agent in self.agents}
        self.truncations = {agent: False for agent in self.agents}
        self.infos = {agent: {"prev_action": None} for agent in self.agents}
        self._agent_selector = agent_selector(self.agents)
        self.agent_selection = self._agent_selector.reset()
        self._last_action = None

        self.seed, self.observation_spaces, self.action_spaces = self._seed(seed=seed)
        (
            self.phase,
            self.players,
            self.num_placed_mats,
            self.largest_bid,
            self.num_active_players,
            self.num_revealed_mats,
        ) = self._init_env_state()

    def step(self, action):
        """
        Renders the environment for outside viewers.
        Takes and executes the action of the agent in the environment, automatically switches control to the next agent.
        """

        """Store current agent."""
        agent = self.agent_selection

        """Store the action for rendering purposes."""
        self._last_action = action

        """Store action as info for the next observation."""
        self.infos[agent] = {
            "prev_action": actions[action] if action != None else action
        }

        """
        Handle if agent is terminated or truncated (i.e. its action is None).
        When an agent is terminated or truncated, it’s removed from agents.
        So if agents list is empty, the env is done.
        """
        if action is None or self.truncations[agent] or self.terminations[agent]:
            """Remove current agent"""
            self.players.pop(agent)
            """Rearrange agents list such that next player is in front and current agent is not included."""
            idx = self.agents.index(agent)
            self.agents = self.agents[idx + 1 :] + self.agents[:idx]

            """
            Check if only 1 player left in the agents list.
            If yes, and it is not yet terminated, then it wins the game as the last standing player.
            """

            """
            Check if only 1 agent left. 
            If yes, and its not terminated, it is the winner.
            Terminate it as well.
            """
            if len(self.agents) == 1 and not self.terminations[self.agents[0]]:
                self.infos[self.agents[0]] = {
                    "debug": "Last player standing. Won the game. Game over."
                }
                self.terminations[self.agents[0]] = True

            """
            Check that agents list did not become empty.
            The next player is the one after the dropped out. 
            Start new round.
            """
            if len(self.agents) != 0:
                self._agent_selector.reinit(self.agents)
                self.agent_selection = self._agent_selector.reset()
                self._round_reset()
            return

        """
        Handle action.
        Modify the _cumulative_reward for the agent.
        From this, the reward for the current turn is calculated.
        """
        action_mask = self._get_action_mask(agent)
        if action_mask[action] == 0:  # invalid
            self.infos[agent] = {"debug": "Invalid action! Agent removed. Round reset."}
            self._cumulative_rewards[agent] -= 1
            self.terminations[agent] = True
            return
        else:  # valid
            """_update() returns True if agent_selector should call next() after as usual,
            or returns False, if the control switching is already handled as a special case inside _update().
            """
            if not self._update(agent, action):
                return

        """Switch control to the next agent. Skip players who passed."""
        self.agent_selection = self._agent_selector.next()
        while self.players[self.agent_selection]["passed"]:
            self.agent_selection = self._agent_selector.next()

    def observe(self, agent):
        """
        Returns the observation an agent currently can make.
        """

        """Create an action mask for the agent with the legal actions based on current state."""
        action_mask = self._get_action_mask(agent)

        """Create an observation for the agent which only contains the observable part of the env state."""
        observation_state = self._create_observation_state()
        observation_vector = self._convert_to_one_hot_vector(observation_state)

        return {"observation": observation_vector, "action_mask": action_mask}

    def render(self):
        """
        Returns the state of the environment using the render mode specified at initialization.
        It is meant to capture the state of the env before the step() is called.
        It should represent the state in the way the agent have seen it before it chose the action.
        """

        if self.render_mode is None:
            return
        elif self.render_mode == "gui":
            return self._render_gui()
        elif self.render_mode == "text":
            return self._render_text()

    def close(self):
        """Closes the rendering window if necessary."""
        pass

    def observation_space(self, agent):
        """
        This function retrieves the observation space for a particular agent. This space should never change for a particular agent ID.
        """
        return self.observation_spaces[agent]

    def action_space(self, agent):
        """
        This function retrieves the action space for a particular agent. This space should never change for a particular agent ID.
        """
        return self.action_spaces[agent]

    """HELPER FUNCTIONS BELOW"""

    def _render_gui(self):
        agent = self.agent_selection
        observation = self.observe(agent)
        game_state = [
            agent,
            (actions[self._last_action] if self._last_action != None else "None"),
            self.infos[agent],
            self._cumulative_rewards[agent],
            self.terminations[agent],
            self.truncations[agent],
            self.phase.name,
            self.num_placed_mats,
            self.largest_bid,
            self.num_active_players,
            self.num_revealed_mats,
            self.agents,
            self.players,
            [
                actions[key]
                for key in list(compress(actions, observation["action_mask"]))
            ],
        ]

        gui.call_gui(game_state)

        return game_state

    def _render_text(self):
        # Make a concat text about the state, and return it
        # The render() function should be called from main, and the user
        # should decide what to do with the received value (e.g. print)
        agent = self.agent_selection

        state = ""
        state += "[-- PLAYER{}--]\n".format(agent)
        state += "|** LAST ACTION **\n"
        state += "| \n"
        state += "| " + (
            actions[self._last_action] if self._last_action != None else "None"
        )
        state += "\n"
        state += "|------------------------------\n"
        state += "|** EFFECTS OF PLAYER'S PREVIOUS ACTION **\n"
        state += "|\n"
        state += "| Info: {}\n".format(self.infos[agent])
        state += "| Reward: {}\n".format(self._cumulative_rewards[agent])
        state += "| Termination: {}\n".format(self.terminations[agent])
        state += "| Truncation: {}\n".format(self.truncations[agent])
        state += "|------------------------------\n"
        state += "|** ENV STATE **\n"
        state += "|\n"
        state += "| Phase: {}\n".format(self.phase.name)
        state += "| Placed mats: {}\n".format(self.num_placed_mats)
        state += "| Largest bid: {}\n".format(self.largest_bid)
        state += "| Active players: {}\n".format(self.num_active_players)
        state += "| Revealed mats: {}\n".format(self.num_revealed_mats)
        state += "| Agents: {}\n".format(self.agents)
        state += "| Players: \n"
        state += pformat(self.players)
        state += "\n"

        observation_state = self._create_observation_state()
        observation = self.observe(agent)

        state += "|------------------------------\n"
        state += "|** OBSERVATION STATE **\n"
        state += "|\n"
        state += str(observation_state)
        state += "\n"
        state += "|------------------------------\n"
        state += "|** OBSERVATION VECTOR **\n"
        state += "|\n"
        state += str(observation["observation"])
        state += "\n"
        state += "|------------------------------\n"
        state += "|** ACTION MASK **\n"
        state += "| " + ", ".join(
            actions[key] for key in list(compress(actions, observation["action_mask"]))
        )
        state += "\n"
        return state

    def _create_observation_state(self):
        observation_state = {
            "phase": self.phase.value,
            "placed_mats": {},
            "mats_in_hand": {},
            "points": {},
            "previous_bids": {},
            "disqualified": {},
        }

        for player_id in range(len(self.possible_agents)):
            player_data = self.players.get(player_id, None)
            if player_data:
                observation_state["disqualified"][player_id] = player_data.get(
                    "disqualified", False
                )
                observation_state["placed_mats"][player_id] = len(
                    player_data.get("placed_mats", [])
                )
                observation_state["mats_in_hand"][player_id] = (
                    player_data.get("skulls", 0),
                    player_data.get("flowers", 0),
                )
                observation_state["points"][player_id] = player_data.get("points", 0)
                observation_state["previous_bids"][player_id] = player_data.get(
                    "bid", 0
                )
            else:
                observation_state["disqualified"][player_id] = True
                observation_state["placed_mats"][player_id] = 0
                observation_state["mats_in_hand"][player_id] = (0, 0)
                observation_state["points"][player_id] = 0
                observation_state["previous_bids"][player_id] = 0

        return observation_state

    def _convert_to_one_hot_vector(self, observation_state):
        num_players = len(self.possible_agents)
        observation = []

        # encode the phase
        phase_one_hot = [0] * 4
        phase_one_hot[observation_state["phase"]] = 1
        observation.extend(phase_one_hot)

        # encode placed mats by each player
        for player_id in range(num_players):
            mats_one_hot = [0] * 4
            if not observation_state["disqualified"][player_id]:
                mats_count = observation_state["placed_mats"][player_id]
                if mats_count != 0:
                    mats_one_hot[mats_count] = 1
            observation.extend(mats_one_hot)

        # encode the number of mats in each player's hand
        for player_id in range(num_players):
            mats_in_hand_one_hot = [0] * 4
            if not observation_state["disqualified"][player_id]:
                skulls_in_hand, flowers_in_hand = observation_state["mats_in_hand"][
                    player_id
                ]
                for i in range(skulls_in_hand + flowers_in_hand):
                    mats_in_hand_one_hot[i] = 1
            observation.extend(mats_in_hand_one_hot)

        # encode the points each player has (binary)
        for player_id in range(num_players):
            if not observation_state["disqualified"][player_id]:
                observation.append(observation_state["points"][player_id])
            else:
                observation.append(0)

        # Encode the previous bids by players
        # observation.extend(observation_state["previous_bids"])

        for player_id in range(num_players):
            if observation_state["previous_bids"][player_id] > 0:
                observation.append(1)
            else:
                observation.append(0)

        return observation

    def _seed(self, seed=None):
        """Seeds the environment for random generation."""
        np.random.seed(seed)

        """
        A dict of the possible observable environment states of every agent, keyed by name.
        This cannot be changed through play or resetting.
        """
        observation_spaces: Dict[Any, Space] = {
            agent: MultiBinary(
                [4 + 2 * 5 * len(self.possible_agents)],
                seed=(count + 1) * seed if seed else None,
            )
            for count, agent in enumerate(self.possible_agents)
        }

        """
        A dict of all the possible actions that the agent can make of every agent, keyed by name.
        This cannot be changed through play or resetting.
        """
        action_spaces: Dict[Any, Space] = {
            agent: Discrete(27, seed=(count + 1) * seed if seed else None)
            for count, agent in enumerate(self.possible_agents)
        }

        return seed, observation_spaces, action_spaces

    def _init_env_state(self):
        phase = Phase.SETUP
        players = {
            agent: {
                "bid": 0,
                "passed": False,
                "points": 0,  # 0 or 1, when it would turn to 2, the player has won
                "skulls": 1,  # shows if the player still have skulls or not
                "flowers": 3,  # shows if the player still have flowers or not
                "placed_mats": [],  # possible items: "skull", "flower"
            }
            for agent in self.agents
        }
        num_placed_mats = 0
        largest_bid = 0
        num_active_players = len(self.agents)
        num_revealed_mats = 0

        return (
            phase,
            players,
            num_placed_mats,
            largest_bid,
            num_active_players,
            num_revealed_mats,
        )

    def _round_reset(self):
        self.phase = Phase.SETUP
        for agent in self.agents:
            self.players[agent]["placed_mats"] = []
            self.players[agent]["passed"] = False
            self.players[agent]["bid"] = 0
        self.num_placed_mats = 0
        self.largest_bid = 0
        self.num_active_players = len(self.agents)
        self.num_revealed_mats = 0

    def _get_action_mask(self, agent):
        """This function returns which actions are valid for a given agent in the current state of the env."""
        action_mask = np.zeros(27, dtype=np.int8)
        player = self.players[agent]
        if self.phase == Phase.SETUP:
            if player["skulls"] - player["placed_mats"].count("skull") > 0:
                action_mask[0] = 1  # Place skull
            if player["flowers"] - player["placed_mats"].count("flower") > 0:
                action_mask[1] = 1  # Place flower
        elif self.phase == Phase.PLACING:
            if player["skulls"] - player["placed_mats"].count("skull") > 0:
                action_mask[0] = 1  # Place skull
            if player["flowers"] - player["placed_mats"].count("flower") > 0:
                action_mask[1] = 1  # Place flower
            if self.largest_bid < self.num_placed_mats:
                action_mask[3 + self.largest_bid : 3 + self.num_placed_mats] = 1  # Bid
        elif self.phase == Phase.BIDDING:
            action_mask[2] = 1  # Pass
            if self.largest_bid < self.num_placed_mats:
                action_mask[3 + self.largest_bid : 3 + self.num_placed_mats] = 1  # Bid
        elif self.phase == Phase.CHALLENGE:
            if len(player["placed_mats"]) > 0:
                action_mask[21 + agent] = 1
            else:
                for other_agent in self.agents:
                    if (
                        other_agent != agent
                        and len(self.players[other_agent]["placed_mats"]) > 0
                    ):
                        action_mask[21 + other_agent] = 1  # Reveal opponents' mat
        return action_mask

    def _update(self, agent, action):
        """Returns True if the agent_selector should step to next, and False if it should not."""
        player = self.players[agent]
        if action == 0:  # Place a Skull
            player["placed_mats"].append("skull")
            self.num_placed_mats += 1
            self._cumulative_rewards[agent] += 1
        elif action == 1:  # Place a Flower
            player["placed_mats"].append("flower")
            self.num_placed_mats += 1
            self._cumulative_rewards[agent] += 1
        elif action == 2:  # Pass
            player["passed"] = True
            self.num_active_players -= 1
            if self.num_active_players == 1:
                self.phase = Phase.CHALLENGE
            self._cumulative_rewards[agent] += 1
        elif action in list(range(3, 21)):  # Bid 1-18
            bid = action - 2
            player["bid"] = bid
            self.largest_bid = bid
            self._cumulative_rewards[agent] += 1

            if self.phase == Phase.PLACING:
                self.phase = Phase.BIDDING

            if self.largest_bid == self.num_placed_mats:
                for other_agent in self.agents:
                    if agent != other_agent:
                        self.players[other_agent]["passed"] = True
                self.phase = Phase.CHALLENGE
        elif action in list(range(21, 27)):  # Reveal 0-5
            reveal_agent = action - 21
            mat = self.players[reveal_agent]["placed_mats"].pop()
            if mat == "skull":
                total_mats = player["skulls"] + player["flowers"]

                # loose a random mat end reset round
                discard = np.random.randint(0, total_mats)
                if discard < player["skulls"]:
                    player["skulls"] -= 1

                    self.infos[agent] = {
                        "debug": "Revealed a skull, discarding a skull. Round reset."
                    }

                    self._cumulative_rewards[agent] -= 5
                else:
                    player["flowers"] -= 1

                    self.infos[agent] = {
                        "debug": "Revealed a skull, discarding a flower. Round reset."
                    }
                    self._cumulative_rewards[agent] -= 3
                hand = player["skulls"] + player["flowers"]
                self._cumulative_rewards[agent] -= 2 ** (4 - hand)

                if total_mats == 1:  # loose game
                    self.terminations[agent] = True
                    self.infos[agent] = {
                        "debug": "Discarded last mat. Agent removed. Round reset."  # Overwrite discard message
                    }
                    self._cumulative_rewards[agent] -= 100

                # set player to start next round
                idx = self.agents.index(agent)
                self.agents = self.agents[idx:] + self.agents[:idx]
                self._agent_selector.reinit(self.agents)
                self.agent_selection = self._agent_selector.reset()
                self._round_reset()
                return False
            elif mat == "flower":
                self.num_revealed_mats += 1
                if self.num_revealed_mats == self.largest_bid:
                    # If player already had a point he wins the game
                    if player["points"] == 1:
                        self.infos[agent] = {
                            "debug": "Collected 2 points. Won the game. Game over."
                        }

                        for a in self.agents:
                            self.terminations[a] = True
                            if a != agent:
                                self.infos[a] = {
                                    "debug": "Lost, another player won. Game over."
                                }
                        self._cumulative_rewards[agent] += 100
                    else:
                        player["points"] += 1
                        self.infos[agent] = {
                            "debug": "Challenge complete. +1 point. Round reset."
                        }
                        self._cumulative_rewards[agent] += 50

                    # set player to start next round
                    idx = self.agents.index(agent)
                    self.agents = self.agents[idx:] + self.agents[:idx]
                    self._agent_selector.reinit(self.agents)
                    self.agent_selection = self._agent_selector.reset()
                    self._round_reset()
                    return False
        else:
            self._cumulative_rewards[agent] += 1
            return True

        if self.phase == Phase.SETUP and self._agent_selector.is_last():
            self.phase = Phase.PLACING

        return True


def env(num_players=0, seed=None, render_mode=None):
    """
    Wrappers provide convenient reusable logic, such as enforcing turn order or clipping out-of-bounds actions.
    """

    env = raw_env(num_players, seed=seed, render_mode=render_mode)

    """
    This wrapper helps error handling for discrete action spaces.
    Asserts if the action given to step is outside of the action space.
    """
    env = wrappers.AssertOutOfBoundsWrapper(env)

    """
    OrderEnforcingWrapper provides a wide vareity of helpful user errors, strongly recommended.
    Checks if function calls or attribute access are in a disallowed order.
        - error on getting rewards, terminations, truncations, infos, agent_selection before reset
        - error on calling step, observe before reset
        - error on iterating without stepping or resetting environment.
        - warn on calling close before render or reset
        - warn on calling step after environment is terminated or truncated
    """
    # env = wrappers.OrderEnforcingWrapper(env)
    return env
